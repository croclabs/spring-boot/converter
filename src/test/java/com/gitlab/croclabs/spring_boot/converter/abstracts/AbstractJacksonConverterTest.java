package com.gitlab.croclabs.spring_boot.converter.abstracts;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.croclabs.spring_boot.converter.impl.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class AbstractJacksonConverterTest {
	Converter c = new Converter();

	JacksonTest test = new JacksonTest("testS", 1, List.of());

	JacksonTest testNestedList = new JacksonTest("testS", 1, List.of(
			new JacksonTest("testS", 1, List.of()),
			new JacksonTest("testS", 1, List.of())
	));

	List<JacksonTest> testList = List.of(testNestedList, testNestedList);

	@Test
	void json() {
		String json = c.json().compose(testList).orElse("");

		Assertions.assertNotEquals("", json);
		Assertions.assertNotNull(c.json().parse(json).orElse(null));
		Assertions.assertNotNull(c.json().parse(json, new TypeReference<List<JacksonTest>>() { }).orElse(null));
		Assertions.assertNotNull(c.json().parse(c.json().compose(test).orElse(""), JacksonTest.class).orElse(null));

		Assertions.assertNull(c.json().parse("a", new TypeReference<JacksonTest>() { }).orElse(null));
		Assertions.assertNull(c.json().parse("a", JacksonTest.class).orElse(null));

		Assertions.assertFalse(c.json().convert(testList, new TypeReference<List<JacksonTestConverted>>() {}).orElse(new ArrayList<>()).isEmpty());
		Assertions.assertNotNull(c.json().convert(testNestedList, JacksonTestConverted.class).orElse(null));
	}

	@Test
	void xml() {
		String xml = c.xml().compose(testList).orElse("");

		Assertions.assertNotEquals("", xml);
		Assertions.assertNotNull(c.xml().parse(xml, new TypeReference<List<JacksonTest>>() { }).orElse(null));
	}

	@Test
	void toml() {
		String toml = c.toml().compose(testNestedList).orElse("");

		Assertions.assertNotEquals("", toml);
		Assertions.assertNotNull(c.toml().parse(toml, new TypeReference<JacksonTest>() { }).orElse(null));
	}

	@Test
	void csv() {
		String csv = c.csv().compose(List.of(test, test)).orElse("");

		Assertions.assertNotEquals("", csv);
		Assertions.assertNotNull(c.csv().parse(csv, JacksonTest.class).orElse(null));

		Assertions.assertEquals("", c.csv().compose(null).orElse(""));
		Assertions.assertEquals("", c.csv().compose(List.of()).orElse(""));
		Assertions.assertNull(c.csv().parse("a", JacksonTest.class).orElse(null));
	}

	@Test
	void yaml() {
		String yaml = c.yaml().compose(testList).orElse("");

		Assertions.assertNotEquals("", yaml);
		Assertions.assertFalse(c.yaml().parse(yaml, new TypeReference<List<JacksonTest>>() {}).orElse(new ArrayList<>()).isEmpty());
	}

	@Test
	void javaProps() {
		String props = c.javaProps().compose(testList).orElse("");

		Assertions.assertNotEquals("", props);
		Assertions.assertFalse(c.javaProps().parse(props, new TypeReference<List<JacksonTest>>() {}).orElse(new ArrayList<>()).isEmpty());
	}
}