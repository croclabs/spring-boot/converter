package com.gitlab.croclabs.spring_boot.converter.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Bool2IntTest {
	Converter c = new Converter();

	@Test
	void parse() {
		Assertions.assertEquals(1, c.bool2int().parse(true).orElse(0));
		Assertions.assertEquals(0, c.bool2int().parse(false).orElse(1));
	}

	@Test
	void compose() {
		Assertions.assertTrue(c.bool2int().compose(1).orElse(false));
		Assertions.assertFalse(c.bool2int().compose(0).orElse(true));

		Assertions.assertFalse(c.bool2int()
				.setup(() -> new Configuration<>(
						(obj) -> obj ? 1 : 0,
						(obj) -> obj > 0
				))
				.compose(-1)
				.orElse(true)
		);
	}
}