package com.gitlab.croclabs.spring_boot.converter.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class String2IntTest {
	Converter c = new Converter();

	@Test
	void parse() {
		Assertions.assertEquals(20, c.string2int().parse("20").orElse(0));
		Assertions.assertEquals(0, c.string2int().parse("20k").orElse(0));
	}

	@Test
	void compose() {
		Assertions.assertEquals("20", c.string2int().compose(20).orElse(""));

		Assertions.assertEquals("", c.string2int()
				.setup(() -> new Configuration<>(
						Integer::parseInt,
						(obj) -> {
							throw new Exception("");
						}
				))
				.compose(20)
				.orElse("")
		);
	}
}