package com.gitlab.croclabs.spring_boot.converter.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConverterExceptionTest {
	@Test
	void construct() {
		Assertions.assertNotNull(new ConverterException());
		Assertions.assertNotNull(new ConverterException("", new Throwable().getCause()));
		Assertions.assertNotNull(new ConverterException(new Throwable().getCause()));
		Assertions.assertNotNull(new ConverterException("", new Throwable().getCause(), true, true));
		Assertions.assertNotNull(new ConverterException(""));
	}

}