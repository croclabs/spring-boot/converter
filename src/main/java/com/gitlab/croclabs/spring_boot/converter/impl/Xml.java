package com.gitlab.croclabs.spring_boot.converter.impl;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractJacksonConverter;

/**
 * Converter for Xml objects
 */
public final class Xml extends AbstractJacksonConverter<Xml, XmlMapper> {
	Xml() {
		super();
	}

	@Override
	protected XmlMapper initMapper() {
		return new XmlMapper();
	}
}
