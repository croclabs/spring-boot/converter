package com.gitlab.croclabs.spring_boot.converter.impl;

// TODO get method that gets the converter by method name, returns IConverter Interface
// TODO avro and smile support (apparently need a schema like csv) --> abstract class for schema based jackson

/**
 * Use an instance of this class to get one of the converters.
 * You can also extend this class to add or override converters.
 */
public class Converter {
	/**
	 * @return new instance of {@link Bool2Int} converter
	 */
	public Bool2Int bool2int() {
		return new Bool2Int();
	}

	/**
	 * @return new instance of {@link String2Int} converter
	 */
	public String2Int string2int() {
		return new String2Int();
	}

	/**
	 * @return new instance of {@link Csv} converter
	 */
	public Csv csv() {
		return new Csv();
	}

	/**
	 * @return new instance of {@link Xml} converter
	 */
	public Xml xml() {
		return new Xml();
	}

	/**
	 * @return new instance of {@link Json} converter
	 */
	public Json json() {
		return new Json();
	}

	/**
	 * @return new instance of {@link Toml} converter
	 */
	public Toml toml() {
		return new Toml();
	}

	/**
	 * @return new instance of {@link YAML} converter
	 */
	public YAML yaml() {
		return new YAML();
	}

	/**
	 * @return new instance of {@link JavaProps} converter
	 */
	public JavaProps javaProps() {
		return new JavaProps();
	}
}
