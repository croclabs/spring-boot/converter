package com.gitlab.croclabs.spring_boot.converter.impl;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractJacksonConverter;

/**
 * Converter for YAML objects
 */
public final class YAML extends AbstractJacksonConverter<YAML, YAMLMapper> {
	YAML() {
		super();
	}

	@Override
	protected YAMLMapper initMapper() {
		return new YAMLMapper();
	}
}
