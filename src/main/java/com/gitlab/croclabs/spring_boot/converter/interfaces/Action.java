package com.gitlab.croclabs.spring_boot.converter.interfaces;

/**
 * Functional interface to do a specific action.
 * Needs to be in a try-catch in case an Exception
 * occurs in the convert process.
 *
 * @param <O> An arbitrary type to consume
 * @param <P> An arbitrary type to supply
 */
@FunctionalInterface
public interface Action<O, P> {
	/**
	 * Action to execute
	 *
	 * @param obj Object to use in action
	 * @return New object after action
	 * @throws Exception Throw an error happens in execution
	 */
	O doAction(P obj) throws Exception;
}
