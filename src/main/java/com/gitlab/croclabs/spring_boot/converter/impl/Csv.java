package com.gitlab.croclabs.spring_boot.converter.impl;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractConverter;
import com.gitlab.croclabs.spring_boot.converter.exceptions.ConverterException;
import com.gitlab.croclabs.spring_boot.converter.interfaces.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Csv converter class
 */
public final class Csv extends AbstractConverter<String, List<?>, Csv> {
	CsvMapper mapper = new CsvMapper();

	Csv() {
		super();

		mapper
				.disable(
						SerializationFeature.FAIL_ON_EMPTY_BEANS
				)
				.disable(
						DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
				)
				.setSerializationInclusion(Include.NON_NULL)
				.setSerializationInclusion(Include.NON_EMPTY);
	}

	/**
	 * Parses String to Type T
	 *
	 * @param obj String to convert
	 * @param clazz {@link Class} to cast to
	 * @return Optional holding the converted object
	 * @param <T> The type to convert to
	 */
	public <T> Optional<List<T>> parse(String obj, Class<T> clazz) {
		try (MappingIterator<T> mit = buildReader(clazz).readValues(obj)) {
			Optional<List<T>> list =  Optional.of(mit.readAll());

			if (list.orElse(new ArrayList<>()).isEmpty()) {
				throw new ConverterException("created list is empty");
			}

			return list;
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	private Action<List<?>, String> parseAction() {
		return obj -> parse(obj, Object.class).orElse(null);
	}

	private Action<String, List<?>> composeAction() {
		return obj -> {
			if (obj == null || obj.isEmpty()) {
				throw new ConverterException("list for csv compose must not be empty nor null!");
			}

			Class<?> clazz = obj.get(0).getClass();
			return buildWriter(clazz).writeValueAsString(obj);
		};
	}

	private <T> ObjectReader buildReader(Class<T> clazz) {
		return mapper.readerFor(clazz)
				.with(mapper.schemaFor(clazz).withHeader().withColumnSeparator(';'));
	}

	private ObjectWriter buildWriter(Class<?> clazz) {
		return mapper.writer(mapper.schemaFor(clazz).withHeader().withColumnSeparator(';'));
	}

	@Override
	protected Configuration<String, List<?>> init() {
		return new Configuration<>(
				parseAction(),
				composeAction()
		);
	}
}
