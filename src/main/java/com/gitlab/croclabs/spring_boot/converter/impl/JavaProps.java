package com.gitlab.croclabs.spring_boot.converter.impl;

import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractJacksonConverter;

/**
 * Converter for Java properties
 */
public final class JavaProps extends AbstractJacksonConverter<JavaProps, JavaPropsMapper> {
	JavaProps() {
		super();
	}

	@Override
	protected JavaPropsMapper initMapper() {
		return new JavaPropsMapper();
	}
}
