package com.gitlab.croclabs.spring_boot.converter.impl;

import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractConverter;

/**
 * {@link Boolean} to {@link Integer} converter
 */
public final class Bool2Int extends AbstractConverter<Boolean, Integer, Bool2Int> {
	Bool2Int() {
		super();
	}

	@Override
	protected Configuration<Boolean, Integer> init() {
		return new Configuration<>(
				(obj) -> obj ? 1 : 0,
				(obj) -> obj != 0
		);
	}
}
