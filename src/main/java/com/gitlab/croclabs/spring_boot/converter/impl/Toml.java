package com.gitlab.croclabs.spring_boot.converter.impl;

import com.fasterxml.jackson.dataformat.toml.TomlMapper;
import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractJacksonConverter;

/**
 * Converter for Toml objects
 */
public final class Toml extends AbstractJacksonConverter<Toml, TomlMapper> {
	Toml() {
		super();
	}

	@Override
	protected TomlMapper initMapper() {
		return new TomlMapper();
	}
}
