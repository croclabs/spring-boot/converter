package com.gitlab.croclabs.spring_boot.converter.interfaces;

import java.util.Optional;

/**
 * Interface for converter classes.
 * <br><br>
 * E.g., a String2Int converter parses to String and
 * composes to Integer.
 *
 * @param <C> The type to compose to
 * @param <P> The type to parse to
 * @param <A> The converter implementation itself
 */
public interface IConverter<C, P, A extends IConverter<C, P, A>> {
	/**
	 * Parses object of type C to type P.
	 *
	 * @param obj Object of type C
	 * @return Optional holding the new object of type P, or empty if exception.
	 */
	Optional<P> parse(C obj);

	/**
	 * Composes object of type P to type C.
	 *
	 * @param obj Object of type P
	 * @return Optional holding the new object of type C, or empty if exception.
	 */
	Optional<C> compose(P obj);
}
