package com.gitlab.croclabs.spring_boot.converter.impl;

import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractConverter;

/**
 * {@link String} to {@link Integer} converter
 */
public final class String2Int extends AbstractConverter<String, Integer, String2Int> {
	String2Int() {
		super();
	}

	@Override
	protected Configuration<String, Integer> init() {
		return new Configuration<>(
				Integer::parseInt,
				Object::toString
		);
	}
}
