package com.gitlab.croclabs.spring_boot.converter.impl;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.gitlab.croclabs.spring_boot.converter.abstracts.AbstractJacksonConverter;

/**
 * Converter for Json objects
 */
public final class Json extends AbstractJacksonConverter<Json, JsonMapper> {
	Json() {
		super();
	}

	@Override
	protected JsonMapper initMapper() {
		return new JsonMapper();
	}
}
