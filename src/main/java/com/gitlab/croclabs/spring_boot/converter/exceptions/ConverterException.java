package com.gitlab.croclabs.spring_boot.converter.exceptions;

/**
 * Exception thrown on converter error
 */
public class ConverterException extends RuntimeException {
	/**
	 *
	 */
	public ConverterException() {
		super();
	}

	/**
	 * @param message message of exception
	 */
	public ConverterException(String message) {
		super(message);
	}

	/**
	 * @param message message of exception
	 * @param cause cause of exception
	 */
	public ConverterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause cause of exception
	 */
	public ConverterException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message message of exception
	 * @param cause cause of exception
	 * @param enableSuppression enable suppression
	 * @param writableStackTrace write stacktrace or not
	 */
	public ConverterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
