package com.gitlab.croclabs.spring_boot.converter.abstracts;

import com.gitlab.croclabs.spring_boot.converter.impl.Configuration;
import com.gitlab.croclabs.spring_boot.converter.interfaces.IConverter;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Abstract class for converter classes. Implements some
 * needed methods in a generic way.
 * <br><br>
 * E.g., a String2Int converter parses to String and
 * composes to Integer.
 *
 * @param <C> The type to compose to
 * @param <P> The type to parse to
 * @param <A> The converter implementation itself
 */
@SuppressWarnings("unchecked")
public abstract class AbstractConverter<C, P, A extends AbstractConverter<C, P, A>>
		implements IConverter<C, P, A> {

	private Configuration<C, P> configuration;

	/**
	 * Constructor for any {@link AbstractConverter}, initializes
	 * the configuration
	 */
	protected AbstractConverter() {
		super();
		this.configuration = init();
	}

	public Optional<P> parse(C obj) {
		try {
			return Optional.of(configuration.getParser().doAction(obj));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	public Optional<C> compose(P obj) {
		try {
			return Optional.of(configuration.getComposer().doAction(obj));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	/**
	 * Used to set up the configuration of this converter
	 *
	 * @param configurationSupplier The supplier used to get the configuration
	 * @return This object
	 */
	public A setup(Supplier<Configuration<C, P>> configurationSupplier) {
		this.configuration = configurationSupplier.get();
		return (A) this;
	}

	/**
	 * Initializes the implementing converter
	 *
	 * @return The configuration to use
	 */
	protected abstract Configuration<C, P> init();
}
