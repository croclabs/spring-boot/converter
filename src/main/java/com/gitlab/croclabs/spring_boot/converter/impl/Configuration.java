package com.gitlab.croclabs.spring_boot.converter.impl;

import com.gitlab.croclabs.spring_boot.converter.interfaces.Action;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Configuration for converters
 *
 * @param <C> Type to compose to
 * @param <P> Type to parse to
 */
@Getter
@AllArgsConstructor
public final class Configuration<C, P> {
	final Action<P, C> parser;
	final Action<C, P> composer;
}
