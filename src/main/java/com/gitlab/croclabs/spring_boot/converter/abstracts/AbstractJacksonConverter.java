package com.gitlab.croclabs.spring_boot.converter.abstracts;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gitlab.croclabs.spring_boot.converter.impl.Configuration;
import com.gitlab.croclabs.spring_boot.converter.interfaces.Action;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Abstract class for Jackson converter classes. Implements
 * some needed methods in a generic way. Also
 * adds support to convert one object into another type.
 * <br><br>
 * E.g., a String2Int converter parses to String and
 * composes to Integer.
 *
 * @param <M> The {@link ObjectMapper} implementation to use
 * @param <A> The converter implementation itself
 */
@SuppressWarnings("unchecked")
public abstract class AbstractJacksonConverter<A extends AbstractJacksonConverter<A, M>, M extends ObjectMapper>
		extends AbstractConverter<String, Object, A> {
	/**
	 * {@link ObjectMapper} of this Jackson converter
	 */
	protected M mapper;

	/**
	 * Constructor for any {@link AbstractJacksonConverter}.
	 * Configures the mapper in this process.
	 */
	protected AbstractJacksonConverter() {
		super();

		configureMapper(m -> m
				.disable(
						SerializationFeature.FAIL_ON_EMPTY_BEANS
				)
				.disable(
						DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
				)
				.setSerializationInclusion(Include.NON_NULL)
				.setSerializationInclusion(Include.NON_EMPTY)
		);
	}

	/**
	 * Configures the given mapper for this
	 * Jackson converter.
	 *
	 * @param consumer {@link Consumer} giving access to this object's mapper
	 * @return This object
	 */
	public A configureMapper(Consumer<M> consumer) {
		consumer.accept(mapper);
		return (A) this;
	}

	@Override
	protected Configuration<String, Object> init() {
		mapper = initMapper();
		mapper.writerWithDefaultPrettyPrinter();
		return new Configuration<>(
				parseAction(),
				composeAction()
		);
	}

	/**
	 * Parses String to Type T
	 *
	 * @param obj String to convert
	 * @param clazz {@link Class} to cast to
	 * @return Optional holding the converted object
	 * @param <T> The type to convert to
	 */
	public <T> Optional<T> parse(String obj, Class<T> clazz) {
		try {
			return Optional.of(mapper.readValue(obj, clazz));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	/**
	 * Parses String to Type T
	 *
	 * @param obj String to convert
	 * @param typeRef {@link TypeReference} to cast to
	 * @return Optional holding the converted object
	 * @param <T> The type to convert to
	 */
	public <T> Optional<T> parse(String obj, TypeReference<T> typeRef) {
		try {
			return Optional.of(mapper.readValue(obj, typeRef));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	/**
	 * {@link Action} used in parse method
	 *
	 * @return {@link Action} to use
	 */
	protected Action<Object, String> parseAction() {
		return obj -> mapper.readValue(obj, new TypeReference<>(){});
	}

	/**
	 * {@link Action} used in compose method
	 *
	 * @return {@link Action} to use
	 */
	protected Action<String, Object> composeAction() {
		return obj -> mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
	}

	/**
	 * Converts Object to Type T
	 *
	 * @param obj object to convert
	 * @param clazz {@link Class} to cast to
	 * @return Optional holding the converted object
	 * @param <T> The type to convert to
	 */
	public <T> Optional<T> convert(Object obj, Class<T> clazz) {
		return parse(compose(obj).orElse(""), clazz);
	}

	/**
	 * Converts Object to Type T
	 *
	 * @param obj object to convert
	 * @param typeRef {@link TypeReference} to cast to
	 * @return Optional holding the converted object
	 * @param <T> The type to convert to
	 */
	public <T> Optional<T> convert(Object obj, TypeReference<T> typeRef) {
		return parse(compose(obj).orElse(""), typeRef);
	}

	/**
	 * Initializes and returns the {@link ObjectMapper}
	 * to use
	 *
	 * @return {@link ObjectMapper} to use
	 */
	protected abstract M initMapper();
}
